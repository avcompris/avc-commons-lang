<?xml version="1.0" encoding="UTF-8" ?>
<!-- 
 !
 !	Copyright Avantage Compris SARL 2008-2009 ©
 !
 !-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
 !
 !	apos
 !
 !-->
<xsl:variable name="apos">'</xsl:variable>

<!--
 !
 !	error.
 !
 !-->
<xsl:template match="*">

	<xsl:message terminate="yes">Don't know how to handle element: <xsl:for-each
		select="ancestor-or-self::*">
	<xsl:value-of select="concat('/', name())"/>
	</xsl:for-each>
	</xsl:message>
	
</xsl:template>

<!--
 !
 !	assert a node exists.
 !
 !-->
<xsl:template name="assertNode">
<xsl:param name="node"/>
<xsl:param name="message"/>

	<xsl:if test="not($node)">
	<xsl:message terminate="yes">
		<xsl:value-of select="$message"/>
	</xsl:message>
	</xsl:if>
	
</xsl:template>

<!--
 !
 !	assert a value is the required one.
 !
 !-->
<xsl:template name="assertEquals">
<xsl:param name="ref"/>
<xsl:param name="actual"/>
<xsl:param name="message"/>

	<xsl:if test="not($actual = $ref)">
	<xsl:message terminate="yes">
		<xsl:value-of select="concat($message, ' Expected: ', $ref, 
			', but was: ', $actual)"/>
	</xsl:message>
	</xsl:if>
	
</xsl:template>

<!--
 !
 !	extract a Java Class' name from a full name
 !
 !-->
<xsl:template name="extract-className">
<xsl:param name="type"/>

	<xsl:choose>
	<xsl:when test="not(contains($type, '.'))">
		<xsl:value-of select="$type"/>
	</xsl:when>
	<xsl:otherwise>
		<!-- terminal recursion -->
		<xsl:call-template name="extract-className">
			<xsl:with-param name="type" select="substring-after($type, '.')"/>
		</xsl:call-template>
	</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>

<!--
 !
 !	extract a Java Class' package name from a full name
 !
 !-->
<xsl:template name="extract-packageName">
<xsl:param name="type"/>
<xsl:param name="prefix"/>

	<xsl:choose>
	<xsl:when test="not(contains($type, '.'))">
		<xsl:value-of select="$prefix"/>
	</xsl:when>
	<xsl:otherwise>
		<!-- terminal recursion -->
		<xsl:call-template name="extract-packageName">
			<xsl:with-param name="type" select="substring-after($type, '.')"/>
			<xsl:with-param name="prefix">
			<xsl:if test="not($prefix = '')">
				<xsl:value-of select="concat($prefix, '.')"/>
			</xsl:if>
			<xsl:value-of select="substring-before($type, '.')"/>
			</xsl:with-param> 
		</xsl:call-template>
	</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>

</xsl:stylesheet>