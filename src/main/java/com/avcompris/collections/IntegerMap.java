package com.avcompris.collections;

import java.io.Serializable;
import java.util.Map;

/**
 * a {@link Map} with mutable integer values.
 * The <code>&lt;T&gt;</code> parameterized type indicates the type of the map keys.
 */
public interface IntegerMap<T> extends Map<T, Integer>, Serializable {

	/**
	 * return a given integer in the map, creating it
	 * if necessary.
	 * 
	 * @param key the key in the map
	 * @return the value, never <code>null</code>.
	 */
	int getInt(T key);

	/**
	 * increment a given integer in the map, creating it (and incremeting it)
	 * if necessary.
	 * 
	 * @param key the key in the map
	 * @return the incremented value.
	 */
	int inc(T key);

	/**
	 * decrement a given integer in the map, creating it (and decremeting it)
	 * if necessary.
	 * 
	 * @param key the key in the map
	 * @return the decremented value.
	 */
	int dec(T key);

	/**
	 * increment a given integer in the map, creating it (and incremeting it)
	 * if necessary.
	 * 
	 * @param key the key in the map
	 * @param increment the delta to add to the value in the map
	 * @return the incremented value.
	 */
	int inc(T key, int increment);

	/**
	 * decrement a given integer in the map, creating it (and decremeting it)
	 * if necessary.
	 * 
	 * @param key the key in the map
	 * @param decrement the delta to susbtract from the value in the map
	 * @return the decremented value.
	 */
	int dec(T key, int decrement);
}
