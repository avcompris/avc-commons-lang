package com.avcompris.lang;

import com.avcompris.common.annotation.Nullable;

/**
 * use this exception class to migrate to Commons Lang 3.
 *
 * @author David Andrianavalontsalama
 */
public final class NullArgumentException extends RuntimeException {

	/**
	 * for serialization.
	 */
	private static final long serialVersionUID = -1645803823506452308L;

	/**
	 * constructor.
	 * 
	 * @param message the error message. Can be null.
	 */
	public NullArgumentException(@Nullable final String message) {

		super(message);
	}
}
