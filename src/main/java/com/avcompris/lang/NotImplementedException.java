package com.avcompris.lang;

import com.avcompris.common.annotation.Nullable;

/**
 * use this exception class to migrate to Commons Lang 3.
 * 
 * @author David Andrianavalontsalama
 */
public final class NotImplementedException extends RuntimeException {

	/**
	 * for serialization.
	 */
	private static final long serialVersionUID = 3710890907678113620L;

	/**
	 * default constructor.
	 */
	public NotImplementedException() {

	}

	/**
	 * constructor with message.
	 * 
	 * @param message the error message. Can be null.
	 */
	public NotImplementedException(@Nullable final String message) {

		super(message);
	}

	/**
	 * constructor with message and cause.
	 * 
	 * @param message the error message. Can be null.
	 * @param cause the cause. Can be null.
	 */
	public NotImplementedException(
			@Nullable final String message,
			@Nullable final Throwable cause) {

		super(message, cause);
	}
}
