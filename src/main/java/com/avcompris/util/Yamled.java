package com.avcompris.util;

/**
 * wrapper for Java objects that represent a YAML structure.
 */
public interface Yamled {

	/**
	 * @param propertyName
	 */
	Yamled get(String propertyName);

	/**
	 * @param propertyName
	 */
	boolean has(String propertyName);

	String label();

	boolean isString();

	String asString();

	boolean is(Class<?> clazz);

	<T> T as(Class<T> clazz);

	boolean isList();
	
	boolean isMap();
	
	boolean isBoolean();

	boolean asBoolean();

	boolean isInt();

	int asInt();

	boolean isLong();

	long asLong();

	boolean isDouble();

	double asDouble();

	/**
	 * @param index
	 */
	Yamled item(int index);

	/**
	 * @param propertyName
	 */
	Yamled[] items(String propertyName);

	Yamled[] items(boolean deep);

	Yamled[] items();

	/**
	 * @param index
	 */
	String string(int index);

	String[] strings();
}
