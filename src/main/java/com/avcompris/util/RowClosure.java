package com.avcompris.util;

/**
 * anonymously implement this interface to have a  
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2009-2010 ©
 */
public interface RowClosure<T, E extends Exception> {

	/**
	 * execute.
	 * 
	 * @param it the closure's parameter.
	 */
	void execute(T it) throws E;
}
