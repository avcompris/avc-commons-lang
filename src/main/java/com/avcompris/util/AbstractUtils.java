package com.avcompris.util;

/**
 * base class for all utility classes, so none of them may be instantiated.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2008-2009 ©
 */
public abstract class AbstractUtils {

	/**
	 * constructor that throws an exception.
	 */
	protected AbstractUtils() {

		throw new InstantiationError("This is an utility class"
				+ " and therefore may not be instantiated.");
	}
}
