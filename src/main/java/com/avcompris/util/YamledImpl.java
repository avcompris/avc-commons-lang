package com.avcompris.util;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newTreeSet;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.avcompris.common.annotation.Nullable;

/**
 * wrapper for Java objects that represent a YAML structure.
 */
class YamledImpl implements Yamled, Serializable {

	/**
	 * for serialization.
	 */
	private static final long serialVersionUID = 859718759086952563L;

	/**
	 * @param yaml the delegate YAML structure.
	 */
	private YamledImpl(final Object yaml) {

		label = null;
		this.yaml = nonNullArgument(yaml, "yaml");
	}

	/**
	 * @param yaml the delegate YAML structure.
	 */
	private YamledImpl(final String key, final Object yaml) {

		label = nonNullArgument(key, "key");
		this.yaml = nonNullArgument(yaml, "yaml");
	}

	/**
	 * In case of <code>items()</code>, we add a meta property, <code>label</code>,
	 * to tell about the key of the singleton-map that holds the underlying map.
	 */
	@Nullable
	private final String label;

	/**
	 * the delegate YAML structure.
	 */
	private final Object yaml;

	@Override
	public Yamled get(final String propertyName) {

		Object y = yaml;

		while (true) {

			final Object value = YamlUtils.getObjectProperty(y, propertyName);

			if (value != null) {
				return wrapToYamled(value);
			}

			if (!Map.class.isInstance(y) || ((Map<?, ?>) y).size() != 1) {
				break;
			}

			y = ((Map<?, ?>) y).values().iterator().next();
		}

		throw new IllegalArgumentException("Cannot find property: " + propertyName + " in yaml: " + yaml);
	}

	@Override
	public boolean has(final String propertyName) {

		Object y = yaml;

		while (true) {

			final boolean has = YamlUtils.hasProperty(y, propertyName);

			if (has) {
				return true;
			}

			if (!Map.class.isInstance(y) || ((Map<?, ?>) y).size() != 1) {
				break;
			}

			y = ((Map<?, ?>) y).values().iterator().next();

			if (!Map.class.isInstance(y)) {
				break;
			}
		}

		return false;
	}

	@Override
	public String label() {

		if (label != null) {

			return label;
		}

		final Map<?, ?> map = (Map<?, ?>) yaml;

		final int size = map.size();

		if (size != 1) {

			throw new IllegalStateException("map.size() should be 1, but was: " + size);
		}

		return (String) map.keySet().iterator().next();
	}

	@Override
	public Yamled[] items(final String propertyName) {

		Object y = yaml;

		while (true) {

			final Collection<?> list = YamlUtils.getListProperty(y, propertyName);

			if (list != null) {
				return getList(list);
			}

			if (!Map.class.isInstance(y) || ((Map<?, ?>) y).size() != 1) {
				break;
			}

			y = ((Map<?, ?>) y).values().iterator().next();
		}

		throw new IllegalArgumentException("Cannot find property: " + propertyName + " in yaml: " + yaml);
	}

	private Yamled[] getList(final Collection<?> list) {

		final Yamled[] yamledArray = new Yamled[list.size()];

		int i = 0;

		for (final Object object : list) {

			yamledArray[i] = wrapToYamled(object);

			++i;
		}

		return yamledArray;
	}

	@Override
	public Yamled[] items() {

		return items(true);
	}

	@Override
	public Yamled[] items(final boolean deep) {

		Object y = yaml;

		while (true) {

			if (List.class.isInstance(y)) {
				return getList((List<?>) y);
			}

			if (!deep || !Map.class.isInstance(y) || ((Map<?, ?>) y).size() != 1) {
				break;
			}

			y = ((Map<?, ?>) y).values().iterator().next();
		}

		if (Map.class.isInstance(y)) {

			final Map<?, ?> map = (Map<?, ?>) y;

			final Yamled[] items = new Yamled[map.size()];

			final Set<String> keys = newTreeSet();

			for (final Object key : map.keySet()) {

				keys.add((String) key);
			}

			int i = 0;

			for (final String key : keys) {

				final Object value = map.get(key);

				items[i] = new YamledImpl(key, value);

				++i;
			}

			return items;
		}

		throw new IllegalArgumentException("Cannot find list in yaml: " + yaml);
	}

	@Override
	public String string(int index) {

		return strings()[index];
	}

	@Override
	public String[] strings() {

		final Yamled[] values = items();

		final String[] array = new String[values.length];

		for (int i = 0; i < array.length; ++i) {

			array[i] = values[i].asString();
		}

		return array;
	}

	@Override
	public int hashCode() {

		return yaml.hashCode();
	}

	@Override
	public boolean equals(final Object o) {

		if (o == null) {
			return false;
		}

		if (getClass().equals(o.getClass())) {
			return yaml.equals(((YamledImpl) o).yaml);
		}

		return false;
	}

	@Override
	public String toString() {

		return yaml.toString();
	}

	@Override
	public String asString() {

		if (String.class.isInstance(yaml)) {

			return (String) yaml;
		}

		if (Integer.class.isInstance(yaml) //
				|| Short.class.isInstance(yaml) //
				|| Long.class.isInstance(yaml) //
				|| Double.class.isInstance(yaml) //
				|| Float.class.isInstance(yaml) //
				|| Boolean.class.isInstance(yaml) //
				|| Byte.class.isInstance(yaml) //
				|| Character.class.isInstance(yaml) //
		) {
			return yaml.toString();
		}

		System.out.println(yaml);
		throw new IllegalStateException(
				"Internal yaml object is not a String or a primitive type: " + yaml.getClass().getName());
	}

	@Override
	public boolean isString() {

		return String.class.isInstance(yaml);
	}

	@Override
	public <T> T as(final Class<T> clazz) {

		checkNotNull(clazz, "clazz");

		if (Integer.class.equals(clazz)) {

			return clazz.cast(asInt());

		} else if (Double.class.equals(clazz)) {

			return clazz.cast(asDouble());

		} else if (Long.class.equals(clazz)) {

			return clazz.cast(asLong());

		} else if (Boolean.class.equals(clazz)) {

			return clazz.cast(asBoolean());
		}

		if (clazz.isEnum()) {

			final Method valueOf_method;

			try {

				valueOf_method = clazz.getMethod("valueOf", String.class);

			} catch (final NoSuchMethodException e) {
				throw new IllegalStateException("Cannot find static method for enum: " + clazz.getName() + ".valueOf()",
						e);
			}

			try {

				return clazz.cast(valueOf_method.invoke(null, (String) yaml));

			} catch (final IllegalAccessException e) {
				throw new RuntimeException("Cannot call " + clazz.getName() + ".valueOf(" + yaml + ")");
			} catch (final InvocationTargetException e) {
				throw new RuntimeException("Cannot call " + clazz.getName() + ".valueOf(" + yaml + ")");
			}
		}

		return clazz.cast(yaml);
	}

	@Override
	public boolean is(final Class<?> clazz) {

		checkNotNull(clazz, "clazz");

		return clazz.isInstance(yaml);
	}

	@Override
	public boolean isList() {

		return is(List.class);
	}

	@Override
	public boolean isMap() {

		return is(Map.class);
	}

	@Override
	public boolean asBoolean() {

		if (Boolean.class.isInstance(yaml)) {

			return (Boolean) yaml;
		}

		throw new IllegalStateException("Internal yaml object is not a Boolean: " + yaml.getClass().getName());
	}

	@Override
	public boolean isBoolean() {

		return Boolean.class.isInstance(yaml);
	}

	@Override
	public int asInt() {

		if (Integer.class.isInstance(yaml)) {

			return (Integer) yaml;

		} else if (Long.class.isInstance(yaml)) {

			return ((Long) yaml).intValue();
		}

		throw new IllegalStateException(
				"Internal yaml object is not an Integer or a Long: " + yaml.getClass().getName());
	}

	@Override
	public boolean isInt() {

		return Integer.class.isInstance(yaml);
	}

	@Override
	public long asLong() {

		if (Integer.class.isInstance(yaml)) {

			return ((Integer) yaml).longValue();

		} else if (Long.class.isInstance(yaml)) {

			return (Long) yaml;
		}

		throw new IllegalStateException(
				"Internal yaml object is not a Long or an Integer: " + yaml.getClass().getName());
	}

	@Override
	public boolean isLong() {

		return Long.class.isInstance(yaml);
	}

	@Override
	public double asDouble() {

		if (Double.class.isInstance(yaml)) {

			return (Double) yaml;

		} else if (Number.class.isInstance(yaml)) {

			return ((Number) yaml).doubleValue();
		}

		throw new IllegalStateException("Internal yaml object is not a Double: " + yaml.getClass().getName());
	}

	@Override
	public boolean isDouble() {

		return Double.class.isInstance(yaml);
	}

	@Override
	public Yamled item(final int index) {

		return items()[index];
	}

	/**
	 * wrap a YAML object to a {@link Yamled} object.
	 */
	@Nullable
	static Yamled wrapToYamled(@Nullable final Object yaml) {

		if (yaml == null) {
			return null;
		}

		// nonNullArgument(yaml, "yaml");

		final Yamled yamled = new YamledImpl(yaml);

		if (Map.class.isInstance(yaml)) {

			return (Yamled) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[] { //
					Yamled.class, Map.class }, new InvocationHandler() {
						@Override
						public Object invoke(final Object proxy, final Method method, final Object[] args)
								throws Throwable {

							if (Map.class.equals(method.getDeclaringClass())) {

								return method.invoke(yaml, args);
							}

							return method.invoke(yamled, args);
						}
					});
		}

		if (List.class.isInstance(yaml)) {

			return (Yamled) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[] { //
					Yamled.class, List.class }, new InvocationHandler() {
						@Override
						public Object invoke(final Object proxy, final Method method, final Object[] args)
								throws Throwable {

							if (List.class.equals(method.getDeclaringClass())) {

								return method.invoke(yaml, args);
							}

							return method.invoke(yamled, args);
						}
					});
		}

		return yamled;
	}
}
