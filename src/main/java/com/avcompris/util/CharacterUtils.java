package com.avcompris.util;

/**
 * utilities on chars.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2008-2009 ©
 */
public abstract class CharacterUtils extends AbstractUtils {

	/**
	 * capitalize one only ASCII character.
	 * 
	 * @param ch the ASCII character
	 * @return the character, uppercase
	 */
	public static char upperCaseFromAscii(
			final char ch) {

		if (ch > 32 && ch < 127) {

			if (ch >= 'a' && ch <= 'z') {

				return (char) (ch + 'A' - 'a');

			} else {

				return ch;
			}
		}

		throw new IllegalArgumentException("Char is not ASCII: " + ch + " ("
				+ ((int) ch) + ")");
	}

	/**
	 * lower-capitalized one only ASCII character.
	 * 
	 * @param ch the ASCII character
	 * @return the character, lowercase
	 */
	public static char lowerCaseFromAscii(
			final char ch) {

		if (ch > 32 && ch < 127) {

			if (ch >= 'A' && ch <= 'Z') {

				return (char) (ch + 'a' - 'A');

			} else {

				return ch;
			}
		}

		throw new IllegalArgumentException("Char is not ASCII: " + ch + " ("
				+ ((int) ch) + ")");
	}
}
