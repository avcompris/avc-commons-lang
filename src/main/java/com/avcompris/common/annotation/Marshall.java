package com.avcompris.common.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Use this annotation to flag a method that can be used to marshal a given
 * instance into a serialized form.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2008-2009 ©
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface Marshall {

	// nothing here
}
