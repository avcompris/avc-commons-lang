package com.avcompris.common.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Use this annotation to flag a method or a class that <b>can</b> be called in
 * a multi-threaded mode.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2008-2009 ©
 */
@Documented
@Retention(RUNTIME)
@Target({
		METHOD, TYPE, FIELD
})
public @interface ThreadSafe {

	// nothing here
}
