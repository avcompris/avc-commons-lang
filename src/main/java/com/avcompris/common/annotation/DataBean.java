package com.avcompris.common.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Use this annotation to flag a class that holds properties and serves them via
 * <code>setters</code> and <code>getters</code> methods. Such a class may not be
 * instantiated directly with <code>new</code>: always use 
 * <code>DataBeans#instantiate(Class)</code> instead.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2008-2009 ©
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface DataBean {

	/**
	 * the property names for this DataBean, ordered as declared.
	 * 
	 * @return the property names
	 */
	String[] value();
}
