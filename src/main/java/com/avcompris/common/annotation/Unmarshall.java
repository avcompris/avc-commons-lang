package com.avcompris.common.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Use this annotation to flag a static method which is an instance factory for
 * a given type from a serialize form (e.g. a {@link String} value).
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2008-2009 ©
 */
@Documented
@Retention(RUNTIME)
@Target({
	METHOD
})
public @interface Unmarshall {

	// nothing here
}
