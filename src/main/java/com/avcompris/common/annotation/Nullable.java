package com.avcompris.common.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.LOCAL_VARIABLE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Use this annotation to flag a method that <em>may</em> return <code>null</code>
 * at runtime, or a parameter that <em>may</em> be <code>null</code> at runtime. No
 * {@link NullPointerException} will be thrown by the container. <br>
 * <br>
 * Please remember instances must be served by the container to be sure
 * annotations such as <code>Nullable</code> will be intercepted and corresponding
 * action fired. E.g. to obtain a new instance of a <code>DataBean</code>, use
 * <code>DataBeans.instantiate(Class)</code> instead of <code>new</code>.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2008-2009 ©
 */
@Documented
@Retention(RUNTIME)
@Target({
		PARAMETER, METHOD, FIELD, LOCAL_VARIABLE
})
public @interface Nullable {

	// nothing here
}
