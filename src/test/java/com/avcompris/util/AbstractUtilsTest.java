package com.avcompris.util;

import org.junit.Test;

/**
 * tests on {@link AbstractUtils}.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2009 ©
 */
public class AbstractUtilsTest {

	@Test(expected = InstantiationError.class)
	public void testConstructor() throws Exception {

		new AbstractUtils() {

			// nothing here
		};
	}
}
