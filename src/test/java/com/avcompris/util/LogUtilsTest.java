package com.avcompris.util;

import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import com.avcompris.util.log.LogUtils;
import com.avcompris.util.log.LogUtils.LogFile;

public class LogUtilsTest {

	@Test
	public void testLogFile() {

		final LogFile log = LogUtils.dumpToFile("toto.log").write(
				"Hello World!");

		final File file = log.getFile();

		assertEquals(new File("target"), file.getParentFile());

		assertEquals(14, file.length());
	}

	@Test
	public void testLogFileThrowable() throws Exception {

		final Throwable e = new IllegalStateException();

		final LogFile log = LogUtils.dumpToFile(e, "toto_throwable.log").write(
				"Hello World!");

		final File file = log.getFile();

		assertEquals(new File("target"), file.getParentFile());

		assertTrue(FileUtils.readFileToString(file, UTF_8).contains(
				e.toString()));
	}
}
