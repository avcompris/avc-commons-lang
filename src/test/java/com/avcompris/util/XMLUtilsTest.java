package com.avcompris.util;

import static com.avcompris.util.XMLUtils.getResourceAsString;
import static com.avcompris.util.XMLUtils.resolveXmlEntitiesToString;
import static com.avcompris.util.XMLUtils.validateSchema;
import static com.avcompris.util.XMLUtils.xmlEntities;
import static org.apache.commons.io.FileUtils.openInputStream;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.StringReader;

import org.apache.commons.digester3.Digester;
import org.junit.Test;

/**
 * tests on the XML utilities.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2008-2009 ©
 */
public class XMLUtilsTest {

	@Test
	public void testGetXmlResourceAsString() throws Exception {

		final String result = getResourceAsString("/test.xml");

		final Digester digester = new Digester();

		digester.parse(new StringReader(result));
	}

	@Test
	public void testResolveXmlEntitiesBlabla() throws Exception {

		assertEquals("bla bla", xmlEntities("bla bla"));
	}

	@Test
	public void testResolveXmlEntitiesLt() throws Exception {

		assertEquals("bla&lt;bla", xmlEntities("bla<bla"));
	}

	@Test
	public void testResolveXmlEntitiesEacuteUnicode() throws Exception {

		assertEquals("bla&#233;bla", xmlEntities("bla\u00E9bla"));
	}

	@Test
	public void testResolveBackXmlEntitiesBlabla() throws Exception {

		assertEquals("bla bla", resolveXmlEntitiesToString("bla bla"));
	}

	@Test
	public void testResolveBackXmlEntitiesLt() throws Exception {

		assertEquals("bla<b\"la",
				resolveXmlEntitiesToString("bla&lt;b&quot;la"));
	}

	@Test
	public void testResolveBackXmlEntitiesEacuteUnicode() throws Exception {

		assertEquals("bla\u00E9bla", resolveXmlEntitiesToString("bla&#233;bla"));
	}

	@Test
	public void testValidateSchema() throws Exception {

		validateSchema(openInputStream(new File(
				"src/test/xml-schema/xmldata.xsd")), new File(
				"src/test/xml/home-001.xml"));
	}
}
