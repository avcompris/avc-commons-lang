package com.avcompris.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.io.File;

import org.junit.Test;

public class Yamled2Test {

	@Test
	public void testGetAsInt() throws Exception {

		final Yamled yaml = YamlUtils.loadYAML(new File("src/test/yaml", "import-sub.yaml"));

		assertEquals(2015, yaml.get("year").asInt());

		assertEquals(2015, yaml.get("year").as(Integer.class).intValue());
	}

	@Test
	public void testGetAsEnum() throws Exception {

		final Yamled yaml = YamlUtils.loadYAML(new File("src/test/yaml", "simple.yaml"));

		assertSame(MyEnum.value, yaml.get("toto").as(MyEnum.class));
	}

	private enum MyEnum {

		value;
	}

	@Test
	public void testGetMapAsItems() throws Exception {

		final Yamled yaml = YamlUtils.loadYAML(new File("src/test/yaml", "map.yaml"));

		final Yamled[] items = yaml.items();

		assertEquals("abc", items[0].label());
		assertEquals("xyz", items[0].asString());

		assertEquals("cde", items[1].label());
		assertEquals("Ada", items[1].get("firstName").asString());
		assertEquals("Lovelace", items[1].get("lastName").asString());

		assertEquals("toto", items[2].label());
		assertEquals("value", items[2].asString());
	}
}
