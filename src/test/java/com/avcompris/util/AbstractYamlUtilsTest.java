package com.avcompris.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.junit.Test;

/**
 * tests on {@link YamlUtils}.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2009 ©
 */
public abstract class AbstractYamlUtilsTest {

	/**
	 * load a YAML object from a file.
	 * 
	 * @param file
	 *            the YAML file to read.
	 * @return the loaded YAML object.
	 */
	protected abstract Object loadYAML(File file) throws IOException;

	@Test
	public final void loadYamlSimple() throws Exception {

		final Object o = loadYAML(new File("src/test/yaml/simple.yaml"));

		assertTrue(Map.class.isAssignableFrom(o.getClass()));

		assertEquals("toto", YamlUtils.getKey(o));
		assertEquals("value", YamlUtils.getValue(o));
	}

	@Test(expected = IllegalArgumentException.class)
	public final void loadYamlDouble_getKey() throws Exception {

		final Object o = loadYAML(new File("src/test/yaml/double.yaml"));

		YamlUtils.getKey(o);
	}

	@Test(expected = IllegalArgumentException.class)
	public final void loadYamlDouble_getValue() throws Exception {

		final Object o = loadYAML(new File("src/test/yaml/double.yaml"));

		YamlUtils.getValue(o);
	}

	@Test
	public final void loadYamlDoubleWithNull() throws Exception {

		final Object o = loadYAML(new File(
				"src/test/yaml/double_with_null.yaml"));

		assertTrue(Map.class.isAssignableFrom(o.getClass()));

		assertEquals("toto", YamlUtils.getKey(o));
		assertEquals("value", YamlUtils.getValue(o));
	}
}
