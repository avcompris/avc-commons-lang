package com.avcompris.util;

import static com.avcompris.util.XPathUtils.arrayEval;
import static com.avcompris.util.XPathUtils.booleanEval;
import static com.avcompris.util.XPathUtils.calcNamespaceMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Map;

import org.junit.Test;

/**
 * tests on {@link XPathUtils}.
 * 
 * @author David Andriana
 */
public class XPathUtilsTest {

	@Test
	public void testBooleanEval() throws Exception {

		assertTrue(booleanEval("<a><b c='c'/></a>", "/a/b/@c = 'c'"));

		assertFalse(booleanEval("<a><b c='c'/></a>", "/a/b/@c = 'd'"));
	}

	@Test
	public void testArrayEval() throws Exception {

		final File source = new File("src/test/xml/pom-sample1.xml");

		final String[] repositories = arrayEval(source,
				"/pom:project/pom:repositories/pom:repository/pom:url", "pom",
				"http://maven.apache.org/POM/4.0.0");

		assertEquals(2, repositories.length);

		assertEquals("http://poc.avcompris.net/nexus/content/groups/public",
				repositories[0]);

		assertEquals(
				"http://poc.avcompris.net/nexus/content/groups/public-snapshots",
				repositories[1]);
	}

	@Test
	public void testCalcNamespaceMap_xmlns_noQuotes() throws Exception {

		final Map<String, String> map = calcNamespaceMap("xmlns:svg=http://www.w3.org/2000/svg");

		assertEquals(1, map.size());

		assertEquals("svg", map.keySet().iterator().next());

		assertEquals("http://www.w3.org/2000/svg", map.values().iterator()
				.next());
	}

	@Test
	public void testCalcNamespaceMap_xmlns_doubleQuotes() throws Exception {

		final Map<String, String> map = calcNamespaceMap("xmlns:svg=\"http://www.w3.org/2000/svg\"");

		assertEquals(1, map.size());

		assertEquals("svg", map.keySet().iterator().next());

		assertEquals("http://www.w3.org/2000/svg", map.values().iterator()
				.next());
	}

	@Test
	public void testCalcNamespaceMap_xmlns_simpleQuotes() throws Exception {

		final Map<String, String> map = calcNamespaceMap("xmlns:svg='http://www.w3.org/2000/svg'");

		assertEquals(1, map.size());

		assertEquals("svg", map.keySet().iterator().next());

		assertEquals("http://www.w3.org/2000/svg", map.values().iterator()
				.next());
	}

	@Test
	public void testCalcNamespaceMap_no_xmlns() throws Exception {

		final Map<String, String> map = calcNamespaceMap("svg",
				"http://www.w3.org/2000/svg");

		assertEquals(1, map.size());

		assertEquals("svg", map.keySet().iterator().next());

		assertEquals("http://www.w3.org/2000/svg", map.values().iterator()
				.next());
	}
}
