package com.avcompris.util;

import static java.util.Locale.ENGLISH;
import static org.apache.commons.lang3.StringUtils.join;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

/**
 * tests on {@link RowClosureUtils}.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2009-2010 ©
 */
public class RowClosureUtilsTest {

	@Test
	public void testCollectOnStringArray() throws Exception {

		final String[] array = RowClosureUtils.collect(new String[]{
				"Riri", "Fifi", "Loulou"
		}, new RowClosureWithReturn<String, String, RuntimeException>() {
			@Override
			public String execute(
					final String s) {

				return s.toUpperCase(ENGLISH);
			}
		});

		assertEquals("RIRI,FIFI,LOULOU", join(array, ','));
	}

	@Test
	public void testCollectOnStringCollection() throws Exception {

		final String[] array = RowClosureUtils.collect(Arrays
				.asList(new String[]{
						"Riri", "Fifi", "Loulou"
				}),
				new RowClosureWithReturn<String, String, RuntimeException>() {
					@Override
					public String execute(
							final String s) {

						return s.toUpperCase(ENGLISH);
					}
				});

		assertEquals("RIRI,FIFI,LOULOU", join(array, ','));
	}
}
