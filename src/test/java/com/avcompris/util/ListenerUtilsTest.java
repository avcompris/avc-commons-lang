package com.avcompris.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * tests on {@link ListenerUtils}.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2008-2009 ©
 */
public class ListenerUtilsTest {

	@Test
	public void testCreateListenerComposite() throws Exception {

		final StringBuffer sb = new StringBuffer();

		final MyListener listener1 = new MyListener() {

			@Override
			public void start() {				
				// do nothing
			}

			@Override
			public void end() {
				// do nothing
			}

			@Override
			public void character(
					final char c) {
				sb.append(c);
			}
		};

		sb.setLength(0);

		assertEquals(0, sb.length());

		parseString("Toto", listener1);

		assertEquals(4, sb.length());

		sb.setLength(0);

		final MyListener composite1 = ListenerUtils.createListenerComposite(
				MyListener.class, listener1);

		parseString("Toto", composite1);

		assertEquals(4, sb.length());

		sb.setLength(0);

		final MyListener composite3 = ListenerUtils.createListenerComposite(
				MyListener.class, listener1, listener1, listener1);

		parseString("Toto", composite3);

		assertEquals(12, sb.length());

		sb.setLength(0);

		final MyListener composite5 = ListenerUtils.createListenerComposite(
				MyListener.class, listener1, composite3, listener1);

		parseString("Toto", composite5);

		assertEquals(20, sb.length());
	}

	/**
	 * parse a String.
	 * 
	 * @param s the String to parse.
	 * @param listener the listener that will be notified with events.
	 */
	private static void parseString(
			final String s,
			final MyListener listener) {

		listener.start();

		for (final char c : s.toCharArray()) {

			listener.character(c);
		}

		listener.end();
	}
}
