package com.avcompris.util;

import static com.avcompris.util.CharacterUtils.lowerCaseFromAscii;
import static com.avcompris.util.CharacterUtils.upperCaseFromAscii;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * tests on {@link CharacterUtils}.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2009 ©
 */
public class CharacterUtilsTest {

	@Test
	public void testAsciiToUpperCase() {

		assertEquals('A', upperCaseFromAscii('a'));
		assertEquals('A', upperCaseFromAscii('A'));
		assertEquals('3', upperCaseFromAscii('3'));
		assertEquals('-', upperCaseFromAscii('-'));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAsciiToUpperCaseCrash() {

		upperCaseFromAscii('é');
	}

	@Test
	public void testAsciiToLowerCase() {

		assertEquals('a', lowerCaseFromAscii('a'));
		assertEquals('a', lowerCaseFromAscii('a'));
		assertEquals('3', lowerCaseFromAscii('3'));
		assertEquals('-', lowerCaseFromAscii('-'));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAsciiToLowerCaseCrash() {

		upperCaseFromAscii('Ê');
	}
}
