package com.avcompris.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

public class YamledTest {

	@Before
	public void setUp() throws Exception {

		yaml = YamlUtils.loadYAML(new File("src/test/yaml/pingmap.yaml"));
	}

	private Yamled yaml;

	@Test
	public void testGetAsString() {

		assertEquals("`curl -s http://bot.whatismyipaddress.com/`",
				yaml.get("myip").asString());

		assertEquals("`curl -s http://bot.whatismyipaddress.com/`",
				yaml.get("myip").as(String.class));
	}

	@Test
	public void testGetItemValuesLength() {

		assertEquals(5, yaml.get("monitoring").item(0).items("http_get").length);
	}

	@Test
	public void testGetItemValuesLabel() {

		assertEquals("AvCompris Jenkins",
				yaml.get("monitoring").item(0).items("http_get")[0].label());
	}

	@Test
	public void testGetItemValuesLength2() {

		assertEquals("AvCompris Nexus",
				yaml.get("monitoring").item(0).items("http_get")[2].label());
	}

	@Test
	public void testGetItemValuesValuesLength() {

		assertEquals(
				1,
				yaml.get("monitoring").item(0).items("http_get")[0].items().length);
	}

	@Test
	public void testGetItemGetItmValuesLength() {

		assertEquals(2, yaml.get("monitoring").item(0).get("http_get").item(3)
				.items().length);
	}

	@Test
	public void testGetItemGetItemItem() {

		assertEquals("http://aduelsheet.info/", yaml.get("monitoring").item(0)
				.get("http_get").item(3).item(0).asString());
	}

	@Test
	public void testGetItemGetItemItem2() {

		assertEquals("http://www.aduelsheet.info/", yaml.get("monitoring")
				.item(0).get("http_get").item(3).item(1).asString());
	}

	@Test
	public void testAsStringYes() {

		assertTrue(yaml.get("monitoring").item(0).get("http_get").item(3)
				.item(1).isString());
	}

	@Test
	public void testAsStringNo() {

		assertFalse(yaml.get("monitoring").item(0).get("http_get").item(3)
				.isString());
	}

	@Test
	public void testGetItemGetItemValues() {

		assertEquals("http://www.aduelsheet.info/", yaml.get("monitoring")
				.item(0).get("http_get").item(3).items()[1].asString());
	}

	@Test
	public void testGetItemGetItemString() {

		assertEquals("http://www.aduelsheet.info/", yaml.get("monitoring")
				.item(0).get("http_get").item(3).string(1));
	}

	@Test
	public void testGetItemGetItemStrings() {

		assertEquals("http://www.aduelsheet.info/", yaml.get("monitoring")
				.item(0).get("http_get").item(3).strings()[1]);
	}
}
