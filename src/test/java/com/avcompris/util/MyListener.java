package com.avcompris.util;

/**
 * listener for tests.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2008-2009 ©
 */
interface MyListener {

	void start();
	
	void character(char c);
	
	void end();
}
