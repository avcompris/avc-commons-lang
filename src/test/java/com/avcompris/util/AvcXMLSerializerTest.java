package com.avcompris.util;

import static org.junit.Assert.assertEquals;

import java.io.StringWriter;
import java.io.Writer;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.helpers.AttributesImpl;

/**
 * tests on {@link AvcXMLSerializer}.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2009 ©
 */
public class AvcXMLSerializerTest {

	@Before
	public void setUp() throws Exception {

		writer = new StringWriter();

		xmlSerializer = new AvcXMLSerializer(writer);
	}

	/**
	 * the writer to use.
	 */
	private Writer writer = null;

	/**
	 * the XMLSerializer to use.
	 */
	private ContentHandler xmlSerializer = null;

	@Test
	public void testSimple() throws Exception {

		xmlSerializer.startDocument();
		xmlSerializer.startElement(null, "toto", null, new AttributesImpl());
		xmlSerializer.endElement(null, "toto", null);
		xmlSerializer.endDocument();

		assertEquals("<toto/>", writer.toString());
	}

	@Test
	public void testSimpleWithAttributes() throws Exception {

		xmlSerializer.startDocument();
		xmlSerializer.startElement(null, "toto", null, newAttributes("abc",
				"def"));
		xmlSerializer.endElement(null, "toto", null);
		xmlSerializer.endDocument();

		assertEquals("<toto\rabc=\"def\"/>", writer.toString());
	}

	@Test
	public void testSimpleWithAttributesAndChars() throws Exception {

		xmlSerializer.startDocument();
		xmlSerializer.startElement(null, "toto", null, newAttributes("abc",
				"def","ghi","jkl"));
		xmlSerializer.characters("tralala".toCharArray(), 0, 7);
		xmlSerializer.endElement(null, "toto", null);
		xmlSerializer.endDocument();

		assertEquals("<toto\rabc=\"def\"\rghi=\"jkl\">tralala</toto>", writer.toString());
	}

	@Test
	public void testSimpleWithChars() throws Exception {

		xmlSerializer.startDocument();
		xmlSerializer.startElement(null, "toto", null, new AttributesImpl());
		xmlSerializer.characters("tralala".toCharArray(), 0, 7);
		xmlSerializer.endElement(null, "toto", null);
		xmlSerializer.endDocument();

		assertEquals("<toto>tralala</toto>", writer.toString());
	}

	@Test
	public void testSimpleWithQuotAttributes() throws Exception {

		xmlSerializer.startDocument();
		xmlSerializer.startElement(null, "toto", null, newAttributes("abc",
				"d\"ef"));
		xmlSerializer.endElement(null, "toto", null);
		xmlSerializer.endDocument();

		assertEquals("<toto\rabc=\"d&quot;ef\"/>", writer.toString());
	}

	@Test
	public void testSimpleWithSpecialChars() throws Exception {

		xmlSerializer.startDocument();
		xmlSerializer.startElement(null, "toto", null, new AttributesImpl());
		xmlSerializer.characters("tr<&ala".toCharArray(), 0, 7);
		xmlSerializer.endElement(null, "toto", null);
		xmlSerializer.endDocument();

		assertEquals("<toto>tr&lt;&amp;ala</toto>", writer.toString());
	}

	@Test
	public void testSimpleWithQuoteChars() throws Exception {

		xmlSerializer.startDocument();
		xmlSerializer.startElement(null, "toto", null, new AttributesImpl());
		xmlSerializer.characters("tr'a\"la".toCharArray(), 0, 7);
		xmlSerializer.endElement(null, "toto", null);
		xmlSerializer.endDocument();

		assertEquals("<toto>tr'a\"la</toto>", writer.toString());
	}

	/**
	 * create Attributes from a list.
	 * 
	 * @param args the attribute name/value pairs.
	 */
	private static Attributes newAttributes(
			final String... args) {

		final AttributesImpl attributes = new AttributesImpl();

		for (int i = 0; i < args.length; i += 2) {

			final String uri = null;
			final String localName = args[i];
			final String qName = null;
			final String type = null;
			final String value = args[i + 1];

			attributes.addAttribute(uri, localName, qName, type, value);
		}
		
		return attributes;
	}
}
