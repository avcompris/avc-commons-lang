package com.avcompris.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.junit.Test;

/**
 * tests on {@link SnakeYAMLUtils}.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2008-2009 ©
 */
public final class SnakeYAMLUtilsTest extends AbstractYamlUtilsTest {

	/**
	 * load a YAML object from a file.
	 * 
	 * @param file the YAML file to read.
	 * @return the loaded YAML object.
	 */
	@Override
	protected Object loadYAML(final File file) throws IOException {

		return SnakeYAMLUtils.loadYAML(file);
	}

	@Test
	public void testImport() throws Exception {

		final Map<?, ?> sub = (Map<?, ?>) SnakeYAMLUtils
				.loadYAML(new File("src/test/yaml", "import-sub.yaml"));

		assertEquals("Nothing important here.", sub.get("message"));
		assertEquals(2015, sub.get("year"));

		final Map<?, ?> main = (Map<?, ?>) SnakeYAMLUtils.loadYAML(
				new File("src/test/yaml", "import-main.yaml"), "-import");

		assertEquals("Hello World!", main.get("message"));
		assertEquals(2015, main.get("year"));
		assertNull(main.get("-import"));
	}

	@Test
	public void testImportNoDirective() throws Exception {

		final Map<?, ?> main = (Map<?, ?>) SnakeYAMLUtils
				.loadYAML(new File("src/test/yaml", "import-main.yaml"));

		assertEquals("Hello World!", main.get("message"));
		assertNull(main.get("year"));
		assertNotNull(main.get("-import"));
	}
}
