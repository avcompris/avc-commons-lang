package com.avcompris.util;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.junit.Test;

/**
 * tests on {@link YamlUtils}.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2008-2009 ©
 */
public final class YamlUtilsTest extends AbstractYamlUtilsTest {

	/**
	 * load a YAML object from a file.
	 * 
	 * @param file
	 *            the YAML file to read.
	 * @return the loaded YAML object.
	 */
	@Override
	protected Object loadYAML(final File file) throws IOException {

		return YamlUtils.loadYAML(file);
	}

	@Test
	public final void loadYamlMultiple() throws Exception {

		final Object[] o = YamlUtils.loadAllYAML(new File(
				"src/test/yaml/multiple.yaml"));

		assertEquals(3, o.length);

		final Map<?, ?> m0 = (Map<?, ?>) o[0];
		final Map<?, ?> m1 = (Map<?, ?>) o[1];
		final Map<?, ?> m2 = (Map<?, ?>) o[2];

		assertEquals("toto", m0.keySet().iterator().next());
		assertEquals("tata", m1.keySet().iterator().next());
		assertEquals("titi", m2.keySet().iterator().next());
	}

	@Test
	public final void loadYamlMultipleMoreSeparators() throws Exception {

		final Object[] o = YamlUtils.loadAllYAML(new File(
				"src/test/yaml/multiple-more-separators.yaml"));

		assertEquals(3, o.length);

		final Map<?, ?> m0 = (Map<?, ?>) o[0];
		final Map<?, ?> m1 = (Map<?, ?>) o[1];
		final Map<?, ?> m2 = (Map<?, ?>) o[2];

		assertEquals("toto", m0.keySet().iterator().next());
		assertEquals("tata", m1.keySet().iterator().next());
		assertEquals("titi", m2.keySet().iterator().next());
	}
}
