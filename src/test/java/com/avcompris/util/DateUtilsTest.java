package com.avcompris.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.joda.time.DateTimeZone;
import org.junit.Test;

/**
 * tests on Joda Time's dates, not only on the AVCOMPRIS's {@link DateUtils}
 * class.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2008-2009 ©
 */
public class DateUtilsTest {

	@Test
	public void testZoneInfoAvailableIDs() throws Exception {

		final Set<?> set = DateTimeZone.getAvailableIDs();

		assertTrue(set.contains("Europe/Paris"));

		assertFalse(set.contains("Paris"));
	}
}
