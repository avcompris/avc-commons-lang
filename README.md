# About avc-commons-lang

This project contains common classes shared by Avantage Compris' open source Java projects.

Its parent project is [avc-commons-parent](https://gitlab.com/avcompris/avc-commons-parent/).

Projects which depend on this one include:

  * [avc-commons-testutil](https://gitlab.com/avcompris/avc-commons-testutil/)
  * [avc-binding-common](https://gitlab.com/avcompris/avc-binding-common/)
  * [avc-binding-dom](https://gitlab.com/avcompris/avc-binding-dom/)
  * [avc-binding-yaml](https://gitlab.com/avcompris/avc-binding-yaml/)

This is the project home page, hosted on GitLab.

[API Documentation is here](https://maven.avcompris.com/avc-commons-lang/apidocs/index.html).

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-commons-lang/)
